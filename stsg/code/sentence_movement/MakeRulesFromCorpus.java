package stsg.code.sentence_movement;

/*
 * 程序中在lib中引入ansj_seg-1.1.jar、stanford-parser-2010-02-26.jar、standfor-parser.jar
 * 可以移除这些jar包，在StanfordParser中修改createTree()函数
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import org.ansj.domain.Term;
import org.ansj.library.UserDefineLibrary;
import org.ansj.splitWord.analysis.ToAnalysis;
import util.FileUtil;
import stsg.code.util.Node;
import stsg.code.util.Rule;
import stsg.code.util.RuleParser;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;

public class MakeRulesFromCorpus {
	private static ArrayList<String> rulepairs = new ArrayList<>();
	private static HashSet<String> util1 = new HashSet<>();
	public static ArrayList<Rule> rulesTreeList = new ArrayList<>();

	public static String ruleTxtPath = "src/stsg/data/sentence_movement/rules.txt";
	public static String sentence1TxtPath = "src/stsg/data/sentence_movement/sentence1.txt";

	public static void main(String[] args) {
		ToAnalysis.parse("");
		StanfordParser.lp = LexicalizedParser
				.loadModel("lib/chinesePCFG.ser.gz");
		InitUserWords();
		System.out.println("making rules into rules.txt");
		// UserInputCorpus();
		FileInputCorpus(); // 从语料库读入文本，生成规则
		System.out.println("end");
	}

	public static void InitUserWords() {
		UserDefineLibrary.insertWord("没有", "userDefine", 1000);
	}

	public static void ImportRules() {
		try {
			MakeRulesFromCorpus.rulesTreeList.clear();
			MakeRulesFromCorpus.rulepairs = FileUtil.readFile(ruleTxtPath,
					"gbk");
			for (int i = 0; i < rulepairs.size(); i += 2) {
				Node source = RuleParser.createTree(rulepairs.get(i));
				Node target = RuleParser.createTree(rulepairs.get(i + 1));
				Rule r = new Rule();
				r.source = source;
				r.target = target;
				MakeRulesFromCorpus.rulesTreeList.add(r);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void FileInputCorpus() {
		try {
			ArrayList<String> corpus = FileUtil.readFile(sentence1TxtPath,
					"gbk");
			if (corpus.size() % 2 != 0) {
				System.out.println("corpus error!");
				System.exit(0);
			}
			for (int i = 0; i < corpus.size(); i += 2) {
				Extracter(corpus.get(i), corpus.get(i + 1));
			}
			System.out.println("rules count:"
					+ MakeRulesFromCorpus.rulepairs.size() / 2);

			FileUtil.writeFile(ruleTxtPath, MakeRulesFromCorpus.rulepairs,
					"gbk");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void UserInputCorpus() {
		java.util.Scanner input = new java.util.Scanner(System.in);
		while (true) {
			String s = input.nextLine();
			String t = input.nextLine();
			if (s.trim().length() == 0) {
				break;
			}
			Extracter(s, t);
		}
		input.close();
	}

	public static void Extracter(String s, String t) {
		java.util.List<Term> sparse = ToAnalysis.parse(s); // ansj工具原句分词List
		java.util.List<Term> tparse = ToAnalysis.parse(t); // ansj工具目标句分词List

		if (sparse.size() != tparse.size()) {
			return;
		}

		String[] source = new String[sparse.size()]; // 原句分词String[]
		for (int i = 0; i < source.length; i++) {
			source[i] = sparse.get(i).getName();
		}
		Node sroot = StanfordParser.createTree(source); // 原句建树
		// System.out.println(StanfordParser.printTree(sroot));

		String[] target = new String[tparse.size()]; // 目标句分词String[]
		for (int i = 0; i < target.length; i++) {
			target[i] = tparse.get(i).getName();
		}
		Node troot = StanfordParser.createTree(target); // 目标句建树

		// 产生规则对
		String s1 = StanfordParser.extractruleWholeTree(sroot);
		String t1 = StanfordParser.extractruleWholeTree(troot);

		String temp = "";
		for (int i = 0; i < target.length - 1; i++) {
			for (int j = i + 1; j < target.length; j++) {
				if (target[i].length() < target[j].length()) {
					temp = target[i];
					target[i] = target[j];
					target[j] = temp;
				}
			}
		}

		int id = 1;
		for (int j = 0; j < target.length; j++) {
			s1 = s1.replace(target[j], "#" + (id));
			t1 = t1.replace(target[j], "#" + (id++));
		}
		// 符合规则输出规则对
		// System.out.println(s1);
		// System.out.println(t1);

		for (int jj = 0; jj < source.length; jj++) {
			s1 = s1.replace(source[jj], "#0");
		}

		String rules = "", rulet = "";
		rules = s1.trim();
		rules = rules.replace("(", " (");
		rules = rules.replace(":", " ");
		rulet = t1.trim();
		rulet = rulet.replace("(", " (");
		rulet = rulet.replace(":", " ");
		if (!util1.contains(rules)) {
			Node ruleS = RuleParser.createTree(rules);
			Node ruleT = RuleParser.createTree(rulet);
			if (RuleParser.isEqualTree(ruleS, ruleT)) {
				// doNothing
			} else {
				MakeRulesFromCorpus.rulepairs.add(rules);
				MakeRulesFromCorpus.rulepairs.add(rulet);

				util1.add(rules);

				// System.out.println(StanfordParser.printTree(ruleS));
				// System.out.println(StanfordParser.printTree(ruleT));
			}
		}
	}
}
